set root_dir        $::env(SNAP_HARDWARE_ROOT)
set action_root     $::env(ACTION_ROOT)
set action_ip_dir   $action_root/ip

set constraintfile $action_root/hw/custom_location_constraint.xdc

# Set IP repository paths
set obj [get_filesets sources_1]
if { $obj != {} } {
   set_property "ip_repo_paths" "[file normalize "/home/jjhoozemans/workspaces/openCAPI/HBM/snappy-hbm-opencapi/workspaces/vivado/ip_repo"]" $obj

   # Rebuild user ip_repo's index before adding any source files
   update_ip_catalog -rebuild
}

#read the user blockdesign
read_bd [ list \
  /home/jjhoozemans/workspaces/openCAPI/HBM/Block_designs/DMA4_smartconnect_capi1024b_HBM4x256b/bd/DMA4_smartconnect_capi1024b_HBM4x256b.bd
]

add_files -fileset constrs_1 -norecurse $constraintfile
  set_property used_in_synthesis false [get_files $constraintfile]
  set_property used_in_implementation true [get_files $constraintfile]
  set_property PROCESSING_ORDER LATE [get_files $constraintfile]


#  /home/jjhoozemans/workspaces/openCAPI/HBM/snappy-hbm-opencapi/snappy_16_unzippers/snappy_16_unzippers.srcs/sources_1/bd/hbm_snappy_dual_2_bd/hbm_snappy_dual_2_bd.bd
