# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "BUS_ADDR_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "BUS_BURST_MAX_LEN" -parent ${Page_0}
  ipgui::add_param $IPINST -name "BUS_BURST_STEP_LEN" -parent ${Page_0}
  ipgui::add_param $IPINST -name "BUS_DATA_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "BUS_LEN_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "CFG" -parent ${Page_0}
  ipgui::add_param $IPINST -name "COMPRESSION_CODEC" -parent ${Page_0}
  ipgui::add_param $IPINST -name "ENCODING" -parent ${Page_0}
  ipgui::add_param $IPINST -name "INDEX_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "TAG_WIDTH" -parent ${Page_0}


}

proc update_PARAM_VALUE.BUS_ADDR_WIDTH { PARAM_VALUE.BUS_ADDR_WIDTH } {
	# Procedure called to update BUS_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.BUS_ADDR_WIDTH { PARAM_VALUE.BUS_ADDR_WIDTH } {
	# Procedure called to validate BUS_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.BUS_BURST_MAX_LEN { PARAM_VALUE.BUS_BURST_MAX_LEN } {
	# Procedure called to update BUS_BURST_MAX_LEN when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.BUS_BURST_MAX_LEN { PARAM_VALUE.BUS_BURST_MAX_LEN } {
	# Procedure called to validate BUS_BURST_MAX_LEN
	return true
}

proc update_PARAM_VALUE.BUS_BURST_STEP_LEN { PARAM_VALUE.BUS_BURST_STEP_LEN } {
	# Procedure called to update BUS_BURST_STEP_LEN when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.BUS_BURST_STEP_LEN { PARAM_VALUE.BUS_BURST_STEP_LEN } {
	# Procedure called to validate BUS_BURST_STEP_LEN
	return true
}

proc update_PARAM_VALUE.BUS_DATA_WIDTH { PARAM_VALUE.BUS_DATA_WIDTH } {
	# Procedure called to update BUS_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.BUS_DATA_WIDTH { PARAM_VALUE.BUS_DATA_WIDTH } {
	# Procedure called to validate BUS_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.BUS_LEN_WIDTH { PARAM_VALUE.BUS_LEN_WIDTH } {
	# Procedure called to update BUS_LEN_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.BUS_LEN_WIDTH { PARAM_VALUE.BUS_LEN_WIDTH } {
	# Procedure called to validate BUS_LEN_WIDTH
	return true
}

proc update_PARAM_VALUE.CFG { PARAM_VALUE.CFG } {
	# Procedure called to update CFG when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.CFG { PARAM_VALUE.CFG } {
	# Procedure called to validate CFG
	return true
}

proc update_PARAM_VALUE.COMPRESSION_CODEC { PARAM_VALUE.COMPRESSION_CODEC } {
	# Procedure called to update COMPRESSION_CODEC when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.COMPRESSION_CODEC { PARAM_VALUE.COMPRESSION_CODEC } {
	# Procedure called to validate COMPRESSION_CODEC
	return true
}

proc update_PARAM_VALUE.ENCODING { PARAM_VALUE.ENCODING } {
	# Procedure called to update ENCODING when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.ENCODING { PARAM_VALUE.ENCODING } {
	# Procedure called to validate ENCODING
	return true
}

proc update_PARAM_VALUE.INDEX_WIDTH { PARAM_VALUE.INDEX_WIDTH } {
	# Procedure called to update INDEX_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.INDEX_WIDTH { PARAM_VALUE.INDEX_WIDTH } {
	# Procedure called to validate INDEX_WIDTH
	return true
}

proc update_PARAM_VALUE.TAG_WIDTH { PARAM_VALUE.TAG_WIDTH } {
	# Procedure called to update TAG_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.TAG_WIDTH { PARAM_VALUE.TAG_WIDTH } {
	# Procedure called to validate TAG_WIDTH
	return true
}


proc update_MODELPARAM_VALUE.BUS_ADDR_WIDTH { MODELPARAM_VALUE.BUS_ADDR_WIDTH PARAM_VALUE.BUS_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.BUS_ADDR_WIDTH}] ${MODELPARAM_VALUE.BUS_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.BUS_DATA_WIDTH { MODELPARAM_VALUE.BUS_DATA_WIDTH PARAM_VALUE.BUS_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.BUS_DATA_WIDTH}] ${MODELPARAM_VALUE.BUS_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.BUS_LEN_WIDTH { MODELPARAM_VALUE.BUS_LEN_WIDTH PARAM_VALUE.BUS_LEN_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.BUS_LEN_WIDTH}] ${MODELPARAM_VALUE.BUS_LEN_WIDTH}
}

proc update_MODELPARAM_VALUE.BUS_BURST_STEP_LEN { MODELPARAM_VALUE.BUS_BURST_STEP_LEN PARAM_VALUE.BUS_BURST_STEP_LEN } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.BUS_BURST_STEP_LEN}] ${MODELPARAM_VALUE.BUS_BURST_STEP_LEN}
}

proc update_MODELPARAM_VALUE.BUS_BURST_MAX_LEN { MODELPARAM_VALUE.BUS_BURST_MAX_LEN PARAM_VALUE.BUS_BURST_MAX_LEN } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.BUS_BURST_MAX_LEN}] ${MODELPARAM_VALUE.BUS_BURST_MAX_LEN}
}

proc update_MODELPARAM_VALUE.INDEX_WIDTH { MODELPARAM_VALUE.INDEX_WIDTH PARAM_VALUE.INDEX_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.INDEX_WIDTH}] ${MODELPARAM_VALUE.INDEX_WIDTH}
}

proc update_MODELPARAM_VALUE.TAG_WIDTH { MODELPARAM_VALUE.TAG_WIDTH PARAM_VALUE.TAG_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.TAG_WIDTH}] ${MODELPARAM_VALUE.TAG_WIDTH}
}

proc update_MODELPARAM_VALUE.CFG { MODELPARAM_VALUE.CFG PARAM_VALUE.CFG } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.CFG}] ${MODELPARAM_VALUE.CFG}
}

proc update_MODELPARAM_VALUE.ENCODING { MODELPARAM_VALUE.ENCODING PARAM_VALUE.ENCODING } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.ENCODING}] ${MODELPARAM_VALUE.ENCODING}
}

proc update_MODELPARAM_VALUE.COMPRESSION_CODEC { MODELPARAM_VALUE.COMPRESSION_CODEC PARAM_VALUE.COMPRESSION_CODEC } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.COMPRESSION_CODEC}] ${MODELPARAM_VALUE.COMPRESSION_CODEC}
}

