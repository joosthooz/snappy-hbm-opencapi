# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "BUS_ADDR_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "BUS_BURST_MAX_LEN" -parent ${Page_0}
  ipgui::add_param $IPINST -name "BUS_BURST_STEP_LEN" -parent ${Page_0}
  ipgui::add_param $IPINST -name "BUS_DATA_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "BUS_LEN_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "INDEX_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "MMIO_ADDR_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "MMIO_DATA_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "NUM_ARROW_BUFFERS" -parent ${Page_0}
  ipgui::add_param $IPINST -name "NUM_REGS" -parent ${Page_0}
  ipgui::add_param $IPINST -name "REG_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "TAG_WIDTH" -parent ${Page_0}


}

proc update_PARAM_VALUE.BUS_ADDR_WIDTH { PARAM_VALUE.BUS_ADDR_WIDTH } {
	# Procedure called to update BUS_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.BUS_ADDR_WIDTH { PARAM_VALUE.BUS_ADDR_WIDTH } {
	# Procedure called to validate BUS_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.BUS_BURST_MAX_LEN { PARAM_VALUE.BUS_BURST_MAX_LEN } {
	# Procedure called to update BUS_BURST_MAX_LEN when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.BUS_BURST_MAX_LEN { PARAM_VALUE.BUS_BURST_MAX_LEN } {
	# Procedure called to validate BUS_BURST_MAX_LEN
	return true
}

proc update_PARAM_VALUE.BUS_BURST_STEP_LEN { PARAM_VALUE.BUS_BURST_STEP_LEN } {
	# Procedure called to update BUS_BURST_STEP_LEN when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.BUS_BURST_STEP_LEN { PARAM_VALUE.BUS_BURST_STEP_LEN } {
	# Procedure called to validate BUS_BURST_STEP_LEN
	return true
}

proc update_PARAM_VALUE.BUS_DATA_WIDTH { PARAM_VALUE.BUS_DATA_WIDTH } {
	# Procedure called to update BUS_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.BUS_DATA_WIDTH { PARAM_VALUE.BUS_DATA_WIDTH } {
	# Procedure called to validate BUS_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.BUS_LEN_WIDTH { PARAM_VALUE.BUS_LEN_WIDTH } {
	# Procedure called to update BUS_LEN_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.BUS_LEN_WIDTH { PARAM_VALUE.BUS_LEN_WIDTH } {
	# Procedure called to validate BUS_LEN_WIDTH
	return true
}

proc update_PARAM_VALUE.INDEX_WIDTH { PARAM_VALUE.INDEX_WIDTH } {
	# Procedure called to update INDEX_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.INDEX_WIDTH { PARAM_VALUE.INDEX_WIDTH } {
	# Procedure called to validate INDEX_WIDTH
	return true
}

proc update_PARAM_VALUE.MMIO_ADDR_WIDTH { PARAM_VALUE.MMIO_ADDR_WIDTH } {
	# Procedure called to update MMIO_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.MMIO_ADDR_WIDTH { PARAM_VALUE.MMIO_ADDR_WIDTH } {
	# Procedure called to validate MMIO_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.MMIO_DATA_WIDTH { PARAM_VALUE.MMIO_DATA_WIDTH } {
	# Procedure called to update MMIO_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.MMIO_DATA_WIDTH { PARAM_VALUE.MMIO_DATA_WIDTH } {
	# Procedure called to validate MMIO_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.NUM_ARROW_BUFFERS { PARAM_VALUE.NUM_ARROW_BUFFERS } {
	# Procedure called to update NUM_ARROW_BUFFERS when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.NUM_ARROW_BUFFERS { PARAM_VALUE.NUM_ARROW_BUFFERS } {
	# Procedure called to validate NUM_ARROW_BUFFERS
	return true
}

proc update_PARAM_VALUE.NUM_REGS { PARAM_VALUE.NUM_REGS } {
	# Procedure called to update NUM_REGS when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.NUM_REGS { PARAM_VALUE.NUM_REGS } {
	# Procedure called to validate NUM_REGS
	return true
}

proc update_PARAM_VALUE.REG_WIDTH { PARAM_VALUE.REG_WIDTH } {
	# Procedure called to update REG_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.REG_WIDTH { PARAM_VALUE.REG_WIDTH } {
	# Procedure called to validate REG_WIDTH
	return true
}

proc update_PARAM_VALUE.TAG_WIDTH { PARAM_VALUE.TAG_WIDTH } {
	# Procedure called to update TAG_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.TAG_WIDTH { PARAM_VALUE.TAG_WIDTH } {
	# Procedure called to validate TAG_WIDTH
	return true
}


proc update_MODELPARAM_VALUE.BUS_ADDR_WIDTH { MODELPARAM_VALUE.BUS_ADDR_WIDTH PARAM_VALUE.BUS_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.BUS_ADDR_WIDTH}] ${MODELPARAM_VALUE.BUS_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.BUS_DATA_WIDTH { MODELPARAM_VALUE.BUS_DATA_WIDTH PARAM_VALUE.BUS_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.BUS_DATA_WIDTH}] ${MODELPARAM_VALUE.BUS_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.BUS_LEN_WIDTH { MODELPARAM_VALUE.BUS_LEN_WIDTH PARAM_VALUE.BUS_LEN_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.BUS_LEN_WIDTH}] ${MODELPARAM_VALUE.BUS_LEN_WIDTH}
}

proc update_MODELPARAM_VALUE.BUS_BURST_MAX_LEN { MODELPARAM_VALUE.BUS_BURST_MAX_LEN PARAM_VALUE.BUS_BURST_MAX_LEN } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.BUS_BURST_MAX_LEN}] ${MODELPARAM_VALUE.BUS_BURST_MAX_LEN}
}

proc update_MODELPARAM_VALUE.BUS_BURST_STEP_LEN { MODELPARAM_VALUE.BUS_BURST_STEP_LEN PARAM_VALUE.BUS_BURST_STEP_LEN } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.BUS_BURST_STEP_LEN}] ${MODELPARAM_VALUE.BUS_BURST_STEP_LEN}
}

proc update_MODELPARAM_VALUE.MMIO_ADDR_WIDTH { MODELPARAM_VALUE.MMIO_ADDR_WIDTH PARAM_VALUE.MMIO_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.MMIO_ADDR_WIDTH}] ${MODELPARAM_VALUE.MMIO_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.MMIO_DATA_WIDTH { MODELPARAM_VALUE.MMIO_DATA_WIDTH PARAM_VALUE.MMIO_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.MMIO_DATA_WIDTH}] ${MODELPARAM_VALUE.MMIO_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.INDEX_WIDTH { MODELPARAM_VALUE.INDEX_WIDTH PARAM_VALUE.INDEX_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.INDEX_WIDTH}] ${MODELPARAM_VALUE.INDEX_WIDTH}
}

proc update_MODELPARAM_VALUE.TAG_WIDTH { MODELPARAM_VALUE.TAG_WIDTH PARAM_VALUE.TAG_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.TAG_WIDTH}] ${MODELPARAM_VALUE.TAG_WIDTH}
}

proc update_MODELPARAM_VALUE.NUM_ARROW_BUFFERS { MODELPARAM_VALUE.NUM_ARROW_BUFFERS PARAM_VALUE.NUM_ARROW_BUFFERS } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.NUM_ARROW_BUFFERS}] ${MODELPARAM_VALUE.NUM_ARROW_BUFFERS}
}

proc update_MODELPARAM_VALUE.NUM_REGS { MODELPARAM_VALUE.NUM_REGS PARAM_VALUE.NUM_REGS } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.NUM_REGS}] ${MODELPARAM_VALUE.NUM_REGS}
}

proc update_MODELPARAM_VALUE.REG_WIDTH { MODELPARAM_VALUE.REG_WIDTH PARAM_VALUE.REG_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.REG_WIDTH}] ${MODELPARAM_VALUE.REG_WIDTH}
}

